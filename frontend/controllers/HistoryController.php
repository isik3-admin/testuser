<?php

namespace frontend\controllers;

use Yii;
use common\models\Balance;
use common\models\Bonus;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class HistoryController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @brief История текущего активного и авторизованного пользователя
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = User::findOne(Yii::$app->user->getId());

        $balance = Balance::findAll(['user_id' => $user->id]);
        $bonus = Bonus::findAll(['user_id' => $user->id]);

        $sumBalance = new Balance();
        $sumBalance = $sumBalance->totalBalUser($user->id);

        $sumBonus = new Bonus();
        $sumBonus = $sumBonus->totalBonUser($user->id);

        return $this->render('index', [
            'balance' => $balance,
            'bonus' => $bonus,
            'sumBalance' => $sumBalance,
            'sumBonus' => $sumBonus,
            'user' => $user,
        ]);
    }

}
