<?php
/* @var $this yii\web\View */
/**
 * @var $balance
 * @var $bonus
 * @var $sumBonus
 * @var $sumBalance
 * @var $user
 */

$this->title = $user->surname . ' ' . $user->name;
?>
<h2><?= $this->title ?></h2>
<h5>Описание: <?= $user->country . ', ' . $user->hobby ?></h5>

<span>Итоговый рублевый баланс: <b><?= $sumBalance[0]->amount ?></b></span>
<table border="1" width="100%" cellpadding="5">
    <caption>История операций баланса</caption>
    <tr>
        <th>Количество</th>
        <th>Назначение</th>
    </tr>
    <?php foreach ($balance as $value) : ?>
        <tr>
            <td><?= $value['amount'] ?></td>
            <td><?= $value['description'] ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<span>Итоговый бонусный баланс: <b><?= $sumBonus[0]->amount ?></b></span>
<table border="1" width="100%" cellpadding="5">
    <caption>История операций бонусов</caption>
    <tr>
        <th>Количество</th>
        <th>Назначение</th>
    </tr>
    <?php foreach ($bonus as $value) : ?>
        <tr>
            <td><?= $value['amount'] ?></td>
            <td><?= $value['description'] ?></td>
        </tr>
    <?php endforeach; ?>
</table>