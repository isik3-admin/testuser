<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'homeUrl' => '/admin',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl'=>'/admin',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'scriptUrl'=>'/admin/index.php',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                // site rules
                '' => 'site/index',
                'login' => 'site/login',

                // user rules
                'user' => 'user/index',
                'user/view/<id:\d+>' => 'user/view',
                'user/update/<id:\d+>' => 'user/update',
                'user/delete/<id:\d+>' => 'user/delete',

                // history rules
                'history/<id:\d+>' => 'history/index',

                // balance rules
                'balance/<id:\d+>' => 'balance/index',
                'balance/add/<id:\d+>' => 'balance/add',
                'balance/down/<id:\d+>' => 'balance/down',
            ],
        ],
    ],
    'params' => $params,
];
