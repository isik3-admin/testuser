<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\Balance;
use common\models\Bonus;
use common\models\BaltoBon;

class DownBalance extends Model
{
    /**
     * @var $user
     */
    public $user;
    /**
     * @var float
     */
    public $amount;
    /**
     * @var string
     */
    public $description;

    /**
     * @inheritDoc
     * @return array
     */
    public function rules()
    {
        return [
            [['amount', 'description'], 'required'],
            [['description'], 'string'],
            [['amount'], 'number', 'min' => 0.01],
            [['amount'], 'totalSumValidate', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @param string $attribute
     * @return void
     */
    public function totalSumValidate($attribute)
    {
        $balance = new Balance();
        $bonus = new Bonus();

        $sum_bal = $balance->totalBalUser($this->user->id);
        $sum_bon = $bonus->totalBonUser($this->user->id);

        $total = ($sum_bal + $sum_bon) - $this->amount;

        if ($total < 0) {
            $this->addError($attribute, 'Введена слишком большая сумма');
        }
    }

    /**
     * @brief  Операция по уменьшению баланса
     * @detailed Функция для автоматического уменьшения бонусного баланса и рублевого баланса,
     *          при условии что баланс пользователя не может уходить в минус
     * @return bool
     */
    public function downBalance()
    {
        $bonus = new Bonus();
        $balance = new Balance();
        $bal_to_bon = new BaltoBon();

        if ($this->validate()) {

            if ($this->totalSumBalAndBon()) {

                if ($this->downBonus() >= 0) {

                    $db = \Yii::$app->db;
                    $transaction = $db->beginTransaction();
                    try {

                        $bonus->user_id = $this->user->id;
                        $bonus->amount = $this->amount * -1;
                        $bonus->description = "Списаны только бонусы в сумме {$this->amount}";
                        $bonus->created_at = time();

                        if ($bonus->save()) {

                            $balance->user_id = $this->user->id;
                            $balance->amount = 0;
                            $balance->description = $this->description;
                            $balance->created_at = time();

                            if ($balance->save()) {
                                $bal_to_bon->balance_id = $balance->id;
                                $bal_to_bon->bonus_id = $bonus->id;
                                $bal_to_bon->created_at = time();

                                if ($bal_to_bon->save()) {
                                    $transaction->commit();

                                    return true;
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        $transaction->rollback();
                    }

                } elseif ($this->downBonus() < 0) {
                    $total = $this->amount - $this->checkBonus();

                    $db = \Yii::$app->db;
                    $transaction = $db->beginTransaction();
                    try {

                        $bonus->user_id = $this->user->id;
                        $bonus->amount = $this->checkBonus() * -1;
                        $bonus->description = ($this->checkBonus() > 0) ? "Списаны все бонусы в сумме {$this->checkBonus()}" :
                                                                            'Бонусов для списания недостаточно.';
                        $bonus->created_at = time();

                        if ($bonus->save()) {

                            $balance->user_id = $this->user->id;
                            $balance->amount = $total * -1;
                            $balance->description = $this->description;
                            $balance->created_at = time();

                            if ($balance->save()) {
                                $bal_to_bon->balance_id = $balance->id;
                                $bal_to_bon->bonus_id = $bonus->id;
                                $bal_to_bon->created_at = time();

                                if ($bal_to_bon->save()) {
                                    $transaction->commit();

                                    return true;
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        $transaction->rollback();
                    }
                }

            }
        }

        return false;
    }

    /**
     * @brief Положительный баланс для операции
     * @detailed Определим достаточно ли бонусов и денег для выполнении списания
     * @return bool
     */
    public function totalSumBalAndBon()
    {
        $balance = new Balance();
        $bonus = new Bonus();

        $sum_bal = $balance->totalBalUser($this->user->id);
        $sum_bon = $bonus->totalBonUser($this->user->id);

        $total = ($sum_bal + $sum_bon) - $this->amount;

        if ($total < 0) {
             return false;
        }

        return true;
    }

    /**
     * @brief Списание бонусов
     * @return float
     */
    public function downBonus()
    {
        $bonus = new Bonus();
        $sum_bon = $bonus->totalBonUser($this->user->id);

        $total = $sum_bon - $this->amount;

        return $total;
    }

    /**
     * @brief Получим сумму бонусов у пользователя
     * @var $check
     * @return bool
     */
    public function checkBonus()
    {
        $bonus = new Bonus();
        $check = $bonus->totalBonUser($this->user->id);

        return $check;
    }
}