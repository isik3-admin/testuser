<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\Balance;
use common\models\Bonus;
use common\models\BaltoBon;

class AddBalance extends Model
{
    /**
     * @var $user
     */
    public $user;
    /**
     * @var float
     */
    public $amount;
    /**
     * @var string
     */
    public $description;

    /**
     * @inheritDoc
     * @return array
     */
    public function rules()
    {
        return [
            [['amount', 'description'], 'required'],
            [['description'], 'string'],
            [['amount'], 'number', 'min' => 0.01],
        ];
    }

    /**
     * @brif Операция по начислению баланса
     * @detailed Зачисление на баланс(рублевой) пользователя и автоматическо зачисление на бонусный баланс
     *          в размере 10% от суммы зачисления на рублевой
     * @var $balance
     * @var $bonus
     * @var $bal_to_bon
     * @return bool
     */
    public function addBalance()
    {
        $balance = new Balance();
        $bonus = new Bonus();
        $bal_to_bon = new BaltoBon();

        if ($this->validate()) {

            $db = \Yii::$app->db;
            $transaction = $db->beginTransaction();
            try {

                $balance->user_id = $this->user->id;
                $balance->amount = $this->amount;
                $balance->description = $this->description;
                $balance->created_at = time();

                if ($balance->save()) {

                    $bonus->user_id = $this->user->id;
                    $bonus->amount = round(($this->amount * 10) / 100);
                    $bonus->description = "Пополнен бонусный баланс пользователя, зачисление автоматических бонусов
                                           от суммы {$this->amount} зачисленного на баланс";
                    $bonus->created_at = time();

                    if ($bonus->save()) {
                        $bal_to_bon->balance_id = $balance->id;
                        $bal_to_bon->bonus_id = $bonus->id;
                        $bal_to_bon->created_at = time();

                        if ($bal_to_bon->save()) {
                            $transaction->commit();

                            return true;
                        }
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollback();
            }
        }

        return false;
    }
}