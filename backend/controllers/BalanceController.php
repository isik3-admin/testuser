<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use common\models\User;
use backend\models\AddBalance;
use backend\models\DownBalance;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class BalanceController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'add', 'down'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'add', 'down'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @brief Страница для добавления или уменьшения баланса пользователя
     * @param $id
     * @return mixed
     */
    public function actionIndex($id)
    {
        $user = $this->findUser($id);

        return $this->render('index', [
            'user' => $user,
        ]);
    }

    /**
     * @brief Добавление баланса пользователю
     * @param $id
     * @return mixed
     */
    public function actionAdd($id)
    {
        $model = new AddBalance();
        $model->user = $this->findUser($id);

        if ($model->load($this->request->post()) && $model->addBalance()) {
            Yii::$app->session->setFlash('success', 'Успешно увеличен баланс.');
            return $this->redirect(['balance/index', 'id' => $id]);
        }

        return $this->render('add', [
            'model' => $model,
        ]);
    }

    /**
     * @brief Списание баланса пользователя
     * @param $id
     * @return mixed
     */
    public function actionDown($id)
    {
        $model = new DownBalance();
        $model->user = $this->findUser($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load($this->request->post()) && $model->downBalance()) {
            Yii::$app->session->setFlash('success', 'Успешно уменьшинь баланс.');
            return $this->redirect(['balance/index', 'id' => $id]);
        }

        return $this->render('down', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUser($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}