<?php

namespace backend\controllers;

use common\models\Balance;
use common\models\Bonus;
use common\models\User;
use common\models\BalToBon;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class HistoryController extends \yii\web\Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @brief Страница отображения истории баланса рублевого и бонусного
     * @param $id
     * @return mixed
     */
    public function actionIndex($id)
    {
        $balance = Balance::find()
            ->select(['*'])
            ->from('balance')
            ->where(['user_id' => $id]);

        $dataProviderBalance = new ActiveDataProvider([
            'query' => $balance,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);

        $bonus = Bonus::find()
            ->select(['*'])
            ->from('bonus')
            ->where(['user_id' => $id]);

        $dataProviderBonus = new ActiveDataProvider([
            'query' => $bonus,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);

        $sumBalance = new Balance();
        $sumBalance = $sumBalance->totalBalUser($id);

        $sumBonus = new Bonus();
        $sumBonus = $sumBonus->totalBonUser($id);

        return $this->render('index', [
            'dataProviderBalance' => $dataProviderBalance,
            'dataProviderBonus' => $dataProviderBonus,
            'sumBalance' => $sumBalance,
            'sumBonus' => $sumBonus,
            'user' => $this->findUser($id),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUser($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
