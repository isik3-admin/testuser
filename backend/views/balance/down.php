<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DownBalance */

$this->title = 'Уменьшить баланс';

?>
<div class="history-of-balance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="history-of-balance-form">

        <?php $form = ActiveForm::begin([
            'id' => 'downBalance-form',
            'enableAjaxValidation' => true,
        ]); ?>

        <?= $form->field($model, 'amount')->textInput()->label('Сумма') ?>

        <?= $form->field($model, 'description')->textarea(['maxlength' => true])->label('Описание назначения') ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>