<?php
/**
 * @brief Страница для добавления или уменьшения баланса пользователя
 * @var $user
 */
use yii\helpers\Html;

$this->title = $user->surname . ' ' . $user->name;
?>
<h1><?= Html::encode($this->title) ?></h1>
<h5>Описание: <?= Html::encode($user->country. ', ' . $user->hobby) ?></h5>
<span><?= Html::a('Добавить баланс пользователю', ['balance/add', 'id' => $user->id])?></span><br>
<span><?= Html::a('Уменьшить баланс пользователя', ['balance/down', 'id' => $user->id])?></span>
