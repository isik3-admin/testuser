<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/**
 * @var $dataProviderBalance
 * @var $dataProviderBonus
 * @var $sumBonus
 * @var $sumBalance
 * @var $user
 */

$this->title = $user->surname . ' ' . $user->name;
?>
<h2><?= $this->title ?></h2>
<h5>Описание: <?= $user->country . ', ' . $user->hobby ?></h5>
<span>Итоговый рублевый баланс: <b><?= $sumBalance ?></b></span>

<?php if ($dataProviderBalance) : ?>

    <?= GridView::widget([
        'dataProvider' => $dataProviderBalance,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Сумма операции',
                'attribute' => 'amount',
                'format' => 'integer',
            ],
            [
                'label' => 'Назначение админом',
                'attribute' => 'description',
                'format' => 'text',
            ],
            [
                'label' => 'Время создания',
                'attribute' => 'created_at',
                'format' => 'datetime',
            ],
        ],
    ]); ?>

    <span>Итоговый бонусный баланс: <b><?= $sumBonus ?></b></span>
    <?= GridView::widget([
        'dataProvider' => $dataProviderBonus,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Сумма операции',
                'attribute' => 'amount',
                'format' => 'integer',
            ],
            [
                'label' => 'Назначение админом',
                'attribute' => 'description',
                'format' => 'text',
            ],
            [
                'label' => 'Время создания',
                'attribute' => 'created_at',
                'format' => 'datetime',
            ],
        ],
    ]); ?>

<?php else : ?>

    <span>Истории у пользователя еще нет.</span>

<?php endif; ?>