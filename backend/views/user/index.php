<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'username',
                'label'=>'Username',
                'format' => 'raw',
                'value'=>function ($data) {
                    return Html::a($data->username,
                        [
                            'history/index',
                            'id' => $data->id
                        ],
                        [
                            'title' => 'Перейти в карточку пользователя',
                            'target' => '_blank',
                        ]
                    );
                },
            ],
            'name',
            'surname',
            'country',
            'hobby',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            [
                'label'=>'Добавить или уменьшить баланс',
                'format' => 'raw',
                'value'=>function ($data) {
                    return Html::a('Перейти',
                        [
                            'balance/index',
                            'id' => $data->id
                        ],
                        [
                                'target' => '_blank',
                        ]
                    );
                },
            ],
            'balance',
            'bonus',
            //'status',
            //'created_at',
            //'updated_at',
            //'verification_token',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]);  ?>


</div>
