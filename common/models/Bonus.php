<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bonus".
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property string $description
 * @property int $created_at
 *
 * @property User $user
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'description', 'created_at'], 'required'],
            [['user_id', 'amount', 'created_at'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя',
            'amount' => 'Сумма',
            'description' => 'Описание',
            'created_at' => 'Время создания',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @brief Итоговая сумма
     * @var $bonSummUser
     * @return int
     */
    public function totalBonUser($id)
    {
        $bonSummUser = Bonus::find()
            ->where(['user_id' => $id])
            ->sum('amount');

        return $bonSummUser;
    }
}
