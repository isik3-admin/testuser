<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bal_to_bon".
 *
 * @property int $id
 * @property int $balance_id
 * @property int $bonus_id
 * @property int $created_at
 *
 * @property Balance $balance
 * @property Bonus $bonus
 */
class BalToBon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bal_to_bon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['balance_id', 'bonus_id', 'created_at'], 'required'],
            [['balance_id', 'bonus_id', 'created_at'], 'integer'],
            [['balance_id'], 'exist', 'skipOnError' => true, 'targetClass' => Balance::className(), 'targetAttribute' => ['balance_id' => 'id']],
            [['bonus_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bonus::className(), 'targetAttribute' => ['bonus_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'balance_id' => 'Balance ID',
            'bonus_id' => 'Bonus ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Balance]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBalance()
    {
        return $this->hasOne(Balance::className(), ['id' => 'balance_id']);
    }

    /**
     * Gets query for [[Bonus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasOne(Bonus::className(), ['id' => 'bonus_id']);
    }
}
