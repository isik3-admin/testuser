<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bal_to_bon}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%balance}}`
 * - `{{%bonus}}`
 */
class m220203_115912_create_bal_to_bon_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bal_to_bon}}', [
            'id' => $this->primaryKey(),
            'balance_id' => $this->integer()->notNull(),
            'bonus_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        // creates index for column `balance_id`
        $this->createIndex(
            '{{%idx-bal_to_bon-balance_id}}',
            '{{%bal_to_bon}}',
            'balance_id'
        );

        // add foreign key for table `{{%balance}}`
        $this->addForeignKey(
            '{{%fk-bal_to_bon-balance_id}}',
            '{{%bal_to_bon}}',
            'balance_id',
            '{{%balance}}',
            'id',
            'CASCADE'
        );

        // creates index for column `bonus_id`
        $this->createIndex(
            '{{%idx-bal_to_bon-bonus_id}}',
            '{{%bal_to_bon}}',
            'bonus_id'
        );

        // add foreign key for table `{{%bonus}}`
        $this->addForeignKey(
            '{{%fk-bal_to_bon-bonus_id}}',
            '{{%bal_to_bon}}',
            'bonus_id',
            '{{%bonus}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%balance}}`
        $this->dropForeignKey(
            '{{%fk-bal_to_bon-balance_id}}',
            '{{%bal_to_bon}}'
        );

        // drops index for column `balance_id`
        $this->dropIndex(
            '{{%idx-bal_to_bon-balance_id}}',
            '{{%bal_to_bon}}'
        );

        // drops foreign key for table `{{%bonus}}`
        $this->dropForeignKey(
            '{{%fk-bal_to_bon-bonus_id}}',
            '{{%bal_to_bon}}'
        );

        // drops index for column `bonus_id`
        $this->dropIndex(
            '{{%idx-bal_to_bon-bonus_id}}',
            '{{%bal_to_bon}}'
        );

        $this->dropTable('{{%bal_to_bon}}');
    }
}
