<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m220202_140707_create_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%balance}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'amount' => $this->decimal(12,2)->notNull(),
            'description' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-balance-user_id}}',
            '{{%balance}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-balance-user_id}}',
            '{{%balance}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-balance-user_id}}',
            '{{%balance}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-balance-user_id}}',
            '{{%balance}}'
        );

        $this->dropTable('{{%balance}}');
    }
}
