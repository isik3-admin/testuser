<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bonus}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m220202_141145_create_bonus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bonus}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'amount' => $this->bigInteger()->notNull(),
            'description' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-bonus-user_id}}',
            '{{%bonus}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-bonus-user_id}}',
            '{{%bonus}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-bonus-user_id}}',
            '{{%bonus}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-bonus-user_id}}',
            '{{%bonus}}'
        );

        $this->dropTable('{{%bonus}}');
    }
}
